use num_bigint::{BigInt, BigUint, ToBigInt};
use num_traits::{One, ToPrimitive, Zero};
use std::collections::HashMap;

/// Extended euclidean algorithm
fn egcd(a: &BigInt, b: &BigInt) -> (BigInt, BigInt, BigInt) {
    if *a == BigInt::zero() {
        (b.clone(), BigInt::zero(), BigInt::one())
    } else {
        let (g, x, y) = egcd(&(b % a), a);
        (g, y - (b / a) * &x, x)
    }
}

pub fn modinverse(a: &BigInt, m: &BigInt) -> Option<BigInt> {
    let (g, x, _) = egcd(a, m);
    if g != BigInt::one() {
        None
    } else {
        Some((x % m + m) % m)
    }
}

pub fn dlog(g: &BigUint, b: &BigUint, modulus: &BigUint) -> Result<Option<BigUint>, String> {
    let m = match modulus.sqrt().to_u32() {
        Some(m) => m,
        None => return Err("Modulus too large!".to_string()),
    };
    let m = m as usize + 1;

    let g_inv = match modinverse(&g.to_bigint().unwrap(), &modulus.to_bigint().unwrap()) {
        Some(g_inv) => g_inv,
        None => return Err(format!("Could not find inverse of {}", &g)),
    };

    let g_minv = g_inv
        .to_biguint()
        .unwrap()
        .modpow(&BigUint::from(m), &modulus);
    let mut lookup = HashMap::with_capacity(m);
    let mut j_ = BigUint::one();
    for j in 0..(m) {
        lookup.insert(j_.clone(), j);
        j_ = j_ * g % modulus;
    }
    // Maybe only go up to m-1?
    lookup.insert(j_.clone(), m);

    let mut y = b.clone();
    for i in 0..m {
        if let Some(j) = lookup.get(&y) {
            return Ok(Some(BigUint::from(i) * m + j));
        }
        y *= &g_minv;
        y %= modulus;
    }
    Ok(None)
}
