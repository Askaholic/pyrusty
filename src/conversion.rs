use num_bigint::BigUint;
use pyo3::exceptions;
use pyo3::ffi;
use pyo3::prelude::*;
use pyo3::types::PyAny;
use pyo3::{AsPyPointer, PyNativeType};
use std::os::raw::{c_int, c_uchar};

#[cfg(target_endian = "little")]
const IS_LITTLE_ENDIAN: c_int = 1;
#[cfg(not(target_endian = "little"))]
const IS_LITTLE_ENDIAN: c_int = 0;

pub struct _BigUint(pub BigUint);

impl IntoPyObject for _BigUint {
    fn into_object(self, py: Python) -> PyObject {
        unsafe {
            let bytes = self.0.to_bytes_le();
            let obj =
                ffi::_PyLong_FromByteArray(bytes.as_ptr() as *const c_uchar, bytes.len(), 1, 0);
            PyObject::from_owned_ptr_or_panic(py, obj)
        }
    }
}
impl<'source> FromPyObject<'source> for _BigUint {
    fn extract(ob: &'source PyAny) -> PyResult<_BigUint> {
        unsafe {
            let num = ffi::PyNumber_Index(ob.as_ptr());
            if num.is_null() {
                return Err(PyErr::fetch(ob.py()));
            }

            let length: usize = match ffi::Py_SIZE(ob.as_ptr()) {
                n if n < 0 => {
                    return Err(exceptions::ValueError::py_err(format!(
                        "N is negative: {}",
                        n
                    )))
                }
                n => (n as usize) * 4, // Py_SIZE * 4 bytes
            };

            let buffer: Vec<c_uchar> = vec![0; length];
            let ok = ffi::_PyLong_AsByteArray(
                ob.as_ptr() as *mut ffi::PyLongObject,
                buffer.as_ptr() as *const c_uchar,
                length,
                IS_LITTLE_ENDIAN,
                0,
            );
            if ok == -1 {
                Err(PyErr::fetch(ob.py()))
            } else {
                if IS_LITTLE_ENDIAN == 1 {
                    Ok(_BigUint(BigUint::from_bytes_le(buffer.as_slice())))
                } else {
                    Ok(_BigUint(BigUint::from_bytes_be(buffer.as_slice())))
                }
            }
        }
    }
}
