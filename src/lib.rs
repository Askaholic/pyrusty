#![feature(test)]

extern crate test;

use num_bigint::{BigUint, RandBigInt};
use num_traits::Num;
use pyo3::exceptions;
use pyo3::prelude::*;
use pyo3::wrap_pyfunction;
use rand::{SeedableRng, StdRng};
use test::Bencher;

mod algorithm;
mod conversion;

use conversion::_BigUint;

#[pyfunction]
fn dlog(g: _BigUint, b: _BigUint, modulus: _BigUint) -> PyResult<Option<_BigUint>> {
    let g = g.0;
    let b = b.0;
    let modulus = modulus.0;

    match algorithm::dlog(&g, &b, &modulus) {
        Err(string) => Err(exceptions::ValueError::py_err(string)),
        Ok(option) => Ok(match option {
            Some(big_int) => Some(_BigUint(big_int)),
            None => None,
        }),
    }
}

#[pymodule]
fn pyrusty(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(dlog))?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_rng() -> StdRng {
        let mut seed = [0; 32];
        for i in 1..32 {
            seed[usize::from(i)] = i;
        }
        SeedableRng::from_seed(seed)
    }


    #[bench]
    fn dlog_bench(b: &mut Bencher) {
        let mut rng = get_rng();
        let m = BigUint::from_str_radix("479001599", 10).unwrap();
        let base = rng.gen_biguint(10);
        let secret = rng.gen_biguint(10);
        let beta = base.modpow(&secret, &m);

        let ans = algorithm::dlog(&base, &beta, &m).unwrap().unwrap();
        assert!(ans == secret);

        b.iter(|| algorithm::dlog(&base, &beta, &m));
    }
}
